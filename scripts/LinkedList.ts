class LinkedList {
    private head: LinkedListNode;
    private tail: LinkedListNode;
    protected current: LinkedListNode;
    private len: number;

    constructor() {
        this.current = new LinkedListNode(null, null);
        this.head = this.current;
        this.tail = this.current;
        this.len = 0;
    }

    public showAppended() {
        document.getElementById("resultText").innerHTML = "Appended " + (<HTMLInputElement>document.getElementById("elem")).value + " to the end of the list!<br><br> Current Node Element Value: " + this.current.getElement() + "<br> Next Node Element Value: " + this.current.getNext().getElement(); 
    }

    public showNode() {
        if (this.current == this.tail) document.getElementById("resultText").innerHTML = "Current Node Element Value: " + this.current.getElement();
        else document.getElementById("resultText").innerHTML = "Current Node Element Value: " + this.current.getElement() + "<br> Next Node Element Value: " + this.current.getNext().getElement();
    }

    public clear() {
        this.head.setNext(null);
        this.current = new LinkedListNode(null, null);
        this.head = this.current;
        this.tail = this.current;
        this.len = 0;
        document.getElementById("resultText").innerHTML = "The list has been cleared!";
    }

    public insert() {
        this.current.setNext(new LinkedListNode((<HTMLInputElement>document.getElementById("elem")).value, this.current.getNext()));
        if (this.tail == this.current) this.tail = this.current.getNext();
        this.len++;
        this.showNode();
    }

    public append() {
        this.tail.setNext(new LinkedListNode((<HTMLInputElement>document.getElementById("elem")).value, null));
        this.tail = this.tail.getNext();
        this.len++;
        this.showAppended();
    }

    public remove() {
        if (this.current.getNext() == null) {
            document.getElementById("resultText").innerHTML = "You cannot remove the list's tail! <br><br>Current Node Element Value: " + this.current.getElement();
            return;
        }
        if (this.tail == this.current.getNext()) this.tail = this.current;
        this.current.setNext(this.current.getNext().getNext());
        this.len--;
        this.showNode();
    }

    public next() {
        if (this.current != this.tail) this.current = this.current.getNext();
        this.showNode();
    }

    public prev() {
        if (this.current == this.head) return;
        let temp: LinkedListNode = this.head;

        while (temp.getNext() != this.current) temp = temp.getNext();
        this.current = temp;
        this.showNode();
    }

    public displayLength() {
        document.getElementById("resultText").innerHTML = "The length of the list is " + this.len + ".";
    }

    public moveToStart() {
        this.current = this.head;
        this.showNode();
    }

    public moveToEnd() {
        this.current = this.tail;
        this.showNode();
    }


    
}