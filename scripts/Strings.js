var Strings = (function () {
    function Strings() {
    }
    Strings.prototype.isPalindrome = function (s) {
        return s == s.split('').reverse().join('');
    };
    Strings.prototype.removeAll = function () {
        var s1 = document.getElementById("stringEntry1").value;
        var s2 = document.getElementById("stringEntry2").value;
        var news1 = s1.split('').filter(function (elem) { return elem.toUpperCase() != s2.toUpperCase(); }).join('');
        document.getElementById("stringEntry1").innerHTML = s1;
        document.getElementById("resultText").innerHTML = "removed all instances of '" + s2 + "' from '" + s1 + "', resulting in '" + news1 + "'!";
    };
    Strings.prototype.checkPalindrome = function () {
        var s1 = document.getElementById("stringEntry1").value;
        var s2 = document.getElementById("stringEntry2").value;
        var result1 = this.isPalindrome(s1);
        var result2 = this.isPalindrome(s2);
        if (result1 && result2) {
            document.getElementById("resultText").innerHTML = "The strings " + s1 + "and " + s2 + " are both palindromes!";
        }
        else if (result1) {
            document.getElementById("resultText").innerHTML = "The string " + s1 + " is a palindrome!";
        }
        else if (result2) {
            document.getElementById("resultText").innerHTML = "The string " + s2 + " is a palindrome!";
        }
        else {
            document.getElementById("resultText").innerHTML = "Neither of the strings are palindromes!";
        }
    };
    Strings.prototype.longestPalindrome = function () {
        //Finds and returns the longest palindrome of s1
    };
    Strings.prototype.anagrams = function () {
        //Determines if s1 and s2 are anagrams of one another
    };
    Strings.prototype.countOccurences = function () {
        //Counts the number of occurences of s2 in s1, given that s2 is a single character.
    };
    return Strings;
}());
//# sourceMappingURL=Strings.js.map