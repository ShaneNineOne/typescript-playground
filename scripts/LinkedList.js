var LinkedList = (function () {
    function LinkedList() {
        this.current = new LinkedListNode(null, null);
        this.head = this.current;
        this.tail = this.current;
        this.len = 0;
    }
    LinkedList.prototype.showAppended = function () {
        document.getElementById("resultText").innerHTML = "Appended " + document.getElementById("elem").value + " to the end of the list!<br><br> Current Node Element Value: " + this.current.getElement() + "<br> Next Node Element Value: " + this.current.getNext().getElement();
    };
    LinkedList.prototype.showNode = function () {
        if (this.current == this.tail)
            document.getElementById("resultText").innerHTML = "Current Node Element Value: " + this.current.getElement();
        else
            document.getElementById("resultText").innerHTML = "Current Node Element Value: " + this.current.getElement() + "<br> Next Node Element Value: " + this.current.getNext().getElement();
    };
    LinkedList.prototype.clear = function () {
        this.head.setNext(null);
        this.current = new LinkedListNode(null, null);
        this.head = this.current;
        this.tail = this.current;
        this.len = 0;
        document.getElementById("resultText").innerHTML = "The list has been cleared!";
    };
    LinkedList.prototype.insert = function () {
        this.current.setNext(new LinkedListNode(document.getElementById("elem").value, this.current.getNext()));
        if (this.tail == this.current)
            this.tail = this.current.getNext();
        this.len++;
        this.showNode();
    };
    LinkedList.prototype.append = function () {
        this.tail.setNext(new LinkedListNode(document.getElementById("elem").value, null));
        this.tail = this.tail.getNext();
        this.len++;
        this.showAppended();
    };
    LinkedList.prototype.remove = function () {
        if (this.current.getNext() == null) {
            document.getElementById("resultText").innerHTML = "You cannot remove the list's tail! <br><br>Current Node Element Value: " + this.current.getElement();
            return;
        }
        if (this.tail == this.current.getNext())
            this.tail = this.current;
        this.current.setNext(this.current.getNext().getNext());
        this.len--;
        this.showNode();
    };
    LinkedList.prototype.next = function () {
        if (this.current != this.tail)
            this.current = this.current.getNext();
        this.showNode();
    };
    LinkedList.prototype.prev = function () {
        if (this.current == this.head)
            return;
        var temp = this.head;
        while (temp.getNext() != this.current)
            temp = temp.getNext();
        this.current = temp;
        this.showNode();
    };
    LinkedList.prototype.displayLength = function () {
        document.getElementById("resultText").innerHTML = "The length of the list is " + this.len + ".";
    };
    LinkedList.prototype.moveToStart = function () {
        this.current = this.head;
        this.showNode();
    };
    LinkedList.prototype.moveToEnd = function () {
        this.current = this.tail;
        this.showNode();
    };
    return LinkedList;
}());
//# sourceMappingURL=LinkedList.js.map