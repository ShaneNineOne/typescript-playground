var LinkedListNode = (function () {
    function LinkedListNode(element, after) {
        this.elem = element;
        this.next = after;
    }
    LinkedListNode.prototype.getElement = function () {
        return this.elem;
    };
    LinkedListNode.prototype.getNext = function () {
        return this.next;
    };
    LinkedListNode.prototype.setNext = function (after) {
        this.next = after;
    };
    return LinkedListNode;
}());
//# sourceMappingURL=LinkedListNode.js.map