class LinkedListNode {
    private elem;
    private next;

    constructor(element, after: LinkedListNode) {
        this.elem = element;
        this.next = after;
    }

    public getElement() {
        return this.elem;
    }

    public getNext() {
        return this.next;
    }

    public setNext(after: LinkedListNode) {
        this.next = after;
    }
}