class Strings {

    private isPalindrome(s: String) {
        return s == s.split('').reverse().join('');
        
    }

    public removeAll() {
        let s1: string = (<HTMLInputElement>document.getElementById("stringEntry1")).value;
        let s2: string = (<HTMLInputElement>document.getElementById("stringEntry2")).value;

        let news1: string = s1.split('').filter(elem => elem.toUpperCase() != s2.toUpperCase()).join('');
        document.getElementById("stringEntry1").innerHTML = s1;
        document.getElementById("resultText").innerHTML = "removed all instances of '" + s2 + "' from '" + s1 + "', resulting in '" + news1 + "'!";
    }

    public checkPalindrome() {
        let s1: string = (<HTMLInputElement>document.getElementById("stringEntry1")).value;
        let s2: string = (<HTMLInputElement>document.getElementById("stringEntry2")).value;
        const result1: boolean = this.isPalindrome(s1);
        const result2: boolean = this.isPalindrome(s2);

        if (result1 && result2) {
            document.getElementById("resultText").innerHTML = "The strings " + s1 + "and " + s2 + " are both palindromes!";
        } else if (result1) {
            document.getElementById("resultText").innerHTML = "The string " + s1 + " is a palindrome!";
        } else if (result2) {
            document.getElementById("resultText").innerHTML = "The string " + s2 + " is a palindrome!";
        } else {
            document.getElementById("resultText").innerHTML = "Neither of the strings are palindromes!";
        }
    }

    public longestPalindrome() {
        //Finds and returns the longest palindrome of s1
    }

    public anagrams() {
        //Determines if s1 and s2 are anagrams of one another
    }

    public countOccurences() {
        //Counts the number of occurences of s2 in s1, given that s2 is a single character.
    }


}